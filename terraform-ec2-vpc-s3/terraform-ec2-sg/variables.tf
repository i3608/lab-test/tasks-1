#############################
# REGION SINGAPORE
#############################

variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_region" {
  type    = string
  default = "ap-southeast-1"
}

variable "vpc_name" {
  type    = string
  default = "VPC1"
}

variable "vpc_cidr" {
  type    = string
  default = "172.160.0.0/16"
}

variable "private_subnets" {
  default = {
    "Private1_bao" = 2
  }
}

variable "public_subnets" {
  default = {
    "Public1_bao" = 1
  }
}
