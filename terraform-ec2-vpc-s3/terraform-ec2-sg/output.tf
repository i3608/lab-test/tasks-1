
# VPC Private Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = aws_subnet.private_subnets
}

# VPC Public Subnets
output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public_subnets
}


# SECURITY GROUP
output "aws_security_group" {
  description = "Thi is Security Group"
  value = aws_security_group.allow_ssh_80_443.id
}



